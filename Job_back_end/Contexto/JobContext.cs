﻿using Job_back_end.Models.Entidades;
using System.Data.Entity;
    

namespace Job_back_end.Contexto
{
    public class JobContext : DbContext
    {
        public DbSet<Job> Jobs { get; set; }
        public DbSet<Task> Tasks { get; set; }

        public JobContext() : base("JobsDB")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
            DropCreateDatabaseIfModelChanges<JobContext> initializer =
            new DropCreateDatabaseIfModelChanges<JobContext>();
            Database.SetInitializer<JobContext>(initializer);
        }

    }
}