﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Job_back_end.Models.Entidades
{
    public class Job
    {
        public int Id { get; set; }
        [Required]
        public string Nome { get; set; }

        public virtual ICollection<Job> JobsRelacionados { get; set; }
        public virtual ICollection<Job> OutrosJobs { get; set; }
        public virtual ICollection<Task> Task { get; set; }

        public Job()
        {
            JobsRelacionados = new HashSet<Job>();
            OutrosJobs = new HashSet<Job>();
        }
    }
}