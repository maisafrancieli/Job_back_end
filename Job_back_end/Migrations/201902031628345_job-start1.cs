namespace Job_back_end.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class jobstart1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Jobs", "Nome", c => c.String(nullable: false));
            AlterColumn("dbo.Tasks", "Nome", c => c.String(nullable: false));
            AlterColumn("dbo.Tasks", "Descricao", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tasks", "Descricao", c => c.String());
            AlterColumn("dbo.Tasks", "Nome", c => c.String());
            AlterColumn("dbo.Jobs", "Nome", c => c.String());
        }
    }
}
