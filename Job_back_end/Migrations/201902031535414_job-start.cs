namespace Job_back_end.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class jobstart : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Jobs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        Descricao = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.JobJobs",
                c => new
                    {
                        Job_Id = c.Int(nullable: false),
                        Job_Id1 = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Job_Id, t.Job_Id1 })
                .ForeignKey("dbo.Jobs", t => t.Job_Id)
                .ForeignKey("dbo.Jobs", t => t.Job_Id1)
                .Index(t => t.Job_Id)
                .Index(t => t.Job_Id1);
            
            CreateTable(
                "dbo.TaskJobs",
                c => new
                    {
                        Task_Id = c.Int(nullable: false),
                        Job_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Task_Id, t.Job_Id })
                .ForeignKey("dbo.Tasks", t => t.Task_Id, cascadeDelete: true)
                .ForeignKey("dbo.Jobs", t => t.Job_Id, cascadeDelete: true)
                .Index(t => t.Task_Id)
                .Index(t => t.Job_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TaskJobs", "Job_Id", "dbo.Jobs");
            DropForeignKey("dbo.TaskJobs", "Task_Id", "dbo.Tasks");
            DropForeignKey("dbo.JobJobs", "Job_Id1", "dbo.Jobs");
            DropForeignKey("dbo.JobJobs", "Job_Id", "dbo.Jobs");
            DropIndex("dbo.TaskJobs", new[] { "Job_Id" });
            DropIndex("dbo.TaskJobs", new[] { "Task_Id" });
            DropIndex("dbo.JobJobs", new[] { "Job_Id1" });
            DropIndex("dbo.JobJobs", new[] { "Job_Id" });
            DropTable("dbo.TaskJobs");
            DropTable("dbo.JobJobs");
            DropTable("dbo.Tasks");
            DropTable("dbo.Jobs");
        }
    }
}
