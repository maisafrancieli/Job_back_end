﻿using Job_back_end.Models.Entidades;
using Job_back_end.Contexto;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Job_back_end.Controllers
{
    public class TaskController : ApiController
    {
        private JobContext _context = new JobContext();

        #region GET METHODS
        [HttpGet]
        public IList<Task> ListaTodasCurtinhas()
        {
            IList<Task> tasks = _context.Tasks.AsQueryable().ToList();
            return tasks.OrderByDescending(c => c.Id).ToList();
        }

        [HttpGet]
        public Task ListaUmTask(int id)
        {
            return _context.Tasks.Find(id);
        }
        #endregion

        #region POST METHODS
        [HttpPost]
        public void AdicionarTask(Task task)
        {
            Task novoTask = new Task()
            {
                Nome = task.Nome,
                Descricao = task.Descricao
            };

            _context.Tasks.Add(novoTask);
            _context.SaveChanges();
        }
        #endregion

        #region PUT METHODS
        [HttpPut]
        public void Editar(Task taskEditado)
        {
            var taskAtual = _context.Tasks.SingleOrDefault(c => c.Id == taskEditado.Id);
            if (taskAtual != null)
            {
                taskAtual.Nome = taskEditado.Nome;
                taskAtual.Descricao = taskEditado.Descricao;

                _context.SaveChanges();
            }
        }

        [HttpPut]
        public Task Editar(long Id)
        {
            Task task = _context.Tasks.SingleOrDefault(c => c.Id == Id);
            return task;
        }
        #endregion

        #region DELETE METHODS
        [HttpDelete]
        public void Excluir(long Id)
        {
            var task = _context.Tasks.SingleOrDefault(c => c.Id == Id);
            if (task != null)
            {
                _context.Tasks.Remove(task);
                _context.SaveChanges();
            }
        }
        #endregion 
    }
}