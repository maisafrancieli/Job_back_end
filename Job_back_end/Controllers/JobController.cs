﻿using Job_back_end.Models.Entidades;
using Job_back_end.Contexto;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;


namespace Job_back_end.Controllers
{
    public class JobController : ApiController
    {
        private JobContext _context = new JobContext();

        #region GET METHODS
        [HttpGet]
        public IList<Job> ListaTodasCurtinhas()
        {
            IList<Job> jobs = _context.Jobs.AsQueryable().ToList();
            return jobs.OrderByDescending(c => c.Id).ToList();
        }

        [HttpGet]
        public Job ListaUmJob(int id)
        {
            return _context.Jobs.Find(id);
        }
        #endregion

        #region POST METHODS
        [HttpPost]
        public void AdicionarJob(Job job)
        {
            Job novoJob = new Job()
            {
                Nome = job.Nome,
            };

            _context.Jobs.Add(novoJob);
            _context.SaveChanges();
        }
        #endregion

        #region PUT METHODS
        [HttpPut]
        public void Editar(Job jobEditado)
        {

          var jobAtual = _context.Jobs.SingleOrDefault(c => c.Id == jobEditado.Id);
          if (jobAtual != null)
          {
                jobAtual.Nome = jobEditado.Nome;
                _context.SaveChanges();
          }
            
        }
        #endregion

        #region DELETE METHODS
        [HttpDelete]
        public void Excluir(long Id)
        {
            var job = _context.Jobs.SingleOrDefault(c => c.Id == Id);
            if (job != null)
            {
                _context.Jobs.Remove(job);
                _context.SaveChanges();
            }
        }
        #endregion   
    }
}