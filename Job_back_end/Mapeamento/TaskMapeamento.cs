﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Job_back_end.Models.Entidades;

namespace Job_back_end.Mapeamento
{
    public class TaskMapeamento : EntityTypeConfiguration<Task>
    {
        public TaskMapeamento()
        {
            ToTable("Task");
            HasKey(item => item.Id).Property(item => item.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(item => item.Nome).IsRequired();
            Property(item => item.Descricao).IsRequired();
        }
    }
}