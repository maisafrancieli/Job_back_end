﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Job_back_end.Models.Entidades;

namespace Job_back_end.Mapeamento
{
    public class JobMapeamento : EntityTypeConfiguration<Job>
    {
        public JobMapeamento()
        {
            ToTable("Job");
            HasKey(item => item.Id).Property(item => item.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            HasMany(item => item.JobsRelacionados).WithMany(item => item.OutrosJobs).Map(item => item.ToTable("JobsRelacionados"));

            Property(item => item.Nome).IsRequired();
        }
    }
}